# Original Phat Tea

_This is the first final blend of Phat Tea we made, and as expected, it's not particularily refined_

 This recipe has non-free components

 This recipe includes products which
are property of Kraft Foods Inc, and 
therefore products made with this recipe not intended for 
retail sale.

**Ingredients:** (makes one gallon)

* 1 gallon of water
* 2 cups of sugar
* 2 packets of Kool-Aid lemonade (pretty
much any type of lemonade works, with 
varying results)
* 20g black tea (~8 tea bags)

**Directions:**

  1. brew the tea in 4 cups of hot water for
5-15 minutes, depending on desired flavor
(you really can't brew it for too long)
  2. while the tea is brewing, add the sugar
and lemonade to a gallon pitcher
  3. after the tea is finished brewing it can
be poured into the pitcher with the other
ingredients (preferably without the teabags)
  4. stir well until everything is dissolved
  5. add ice
  6. fill the pitcher with water, or until
desired strength 
  7. drink the whole gallon (not recommended)
